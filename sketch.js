const tiles = []

const RDL = 0;
const D = 1;
const U = 2;
const L = 3;
const UD = 4;
const RD = 5;
const BLANK = 6;
const URD = 7;
const LR = 8;
const FULL = 9;
const UR = 10;
const LUR = 11;

P_WIDTH = 100;
P_HEIGHT = 100;
DIM = Math.round((window.screen.width-100) / 100);

function preload() {
	tiles[0] = loadImage("./tiles/right-down-left.png");
	tiles[1] = loadImage("./tiles/down.png");
	tiles[2] = loadImage("./tiles/up.png");
	tiles[3] = loadImage("./tiles/left.png");
	tiles[4] = loadImage("./tiles/up-down.png");
	tiles[5] = loadImage("./tiles/right-down.png");
	tiles[6] = loadImage("./tiles/empty.png");
	tiles[7] = loadImage("./tiles/up-right-down.png");
	tiles[8] = loadImage("./tiles/left-right.png");
	tiles[9] = loadImage("./tiles/full.png");
	tiles[10] = loadImage("./tiles/up-right.png");
	tiles[11] = loadImage("./tiles/left-up-right.png");
}

function setup() {
    createCanvas(window.screen.height-100, window.screen.height-100);
    print(DIM)
}

function draw() {
    background(151);
    for(let i = 0; i < DIM; i++) {
        for(let j = 0; j < DIM; j++) {
            w = (i % DIM) * P_WIDTH;
            h = (j % DIM) * P_HEIGHT;
            t = (i % 2 == 0) ? BLANK : FULL;
            image(tiles[t], h, w);
        }
    }
}